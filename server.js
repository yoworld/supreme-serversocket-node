const net = require('net');
const http = require('http');
const {Connection} = require('socket-json-wrapper');
const translate = require('google-translate-api');

var swfClients = {};
var appClients = {};
var debugClients = {};
var syncs = {};

let blockwords = [
    'lmao', 'lol', 'ha', 'lmfao', 'stfu', 'rofl', 'hehe', 'tfw', 'tldr', 'bs'
];


let sendRequest = function (path, headers, method = 'GET', func) {
    return http.request({
        host: 's1.supreme-one.net',
        port: '2052',
        path: path,
        headers: headers,
        method: method,
        json: true
    }, func);
};
let policyXml = '<?xml version="1.0"?>' +
    '<!DOCTYPE cross-domain-policy>' +
    '<cross-domain-policy>' +
    '<site-control permitted-cross-domain-policies="master-only"/>' +
    '<allow-access-from domain="*" to-ports="2002"/>' +
    '</cross-domain-policy>';

function handleAuth(connection, json) {
    if (json.type === "init") {
        connection.__s1_token_acc = json.auth.s1_token_acc;
        connection.__s1_token = json.auth.s1_token;
        connection.__type = json.message;
        syncs[json.auth.s1_token_acc] = [];

        switch (json.message) {
            case "swf":
                sendRequest('/api/v1/s1/account',
                    {
                        Authorization: 'Bearer ' + connection.__s1_token_acc
                    }, 'GET', function (res) {
                        res.setEncoding('utf8');
                        res.on('data', function (body) {
                            connection.__account = JSON.parse(body).data;
                            connection.__account_id = connection.__account.user_id;
                            Object.values(debugClients).forEach(function (entry) {
                                entry.send({
                                    type: 'debug.connected',
                                    message: 'swf',
                                    data: connection.__account
                                });
                            });
                        });
                        res.on('error', function (error) {
                            console.log(error);
                        });
                    }).end();

                sendRequest('/api/v1/s1/user',
                    {
                        Authorization: 'Bearer ' + connection.__s1_token
                    }, 'GET', function (res) {
                        res.setEncoding('utf8');
                        res.on('data', function (body) {
                            connection.__user = JSON.parse(body).data;
                            connection.__user_id = connection.__user.id;
                        });
                        res.on('error', function (error) {
                            console.log(error);
                        });
                    }).end();
                swfClients[json.auth.s1_token_acc] = connection;
                console.log("Swf Client Added");
                return true;
            case "app":
                sendRequest('/api/v1/s1/account',
                    {
                        Authorization: 'Bearer ' + connection.__s1_token_acc
                    }, 'GET', function (res) {
                        res.setEncoding('utf8');
                        res.on('data', function (body) {
                            try {
                                connection.__account = JSON.parse(body).data;
                                connection.__account_id = connection.__account.user_id
                            } catch (e) {
                                console.error(e);
                            }
                        });
                        res.on('error', function (error) {
                            console.error(error);
                        });
                    }).end();

                sendRequest('/api/v1/s1/user',
                    {
                        Authorization: 'Bearer ' + connection.__s1_token
                    }, 'GET', function (res) {
                        res.setEncoding('utf8');
                        res.on('data', function (body) {
                            try {
                                connection.__user = JSON.parse(body).data;
                                connection.__user_id = connection.__user.id;
                                Object.values(debugClients).forEach(function (entry) {
                                    entry.send({
                                        type: 'debug.connected',
                                        message: 'app',
                                        data: connection.__user
                                    });
                                });
                            } catch (e) {
                                console.error(e);
                            }
                        });
                        res.on('error', function (error) {
                            console.error(error);
                        });
                    }).end();
                appClients[json.auth.s1_token] = connection;
                console.log("App Client Added");
                return true;
            default:
                connection.destroy();
                break;
        }
    }
    return false;
}

function handleConnection(connection, appClient, swfClient, json) {
    if (json.type === "music") {
        if (connection.__type === 'swf' || connection.__type === 'app') {
            let room = connection.__room;
            Object.values(swfClients).forEach(function (entry) {
                if (entry.__room.roomId === room.roomId &&
                    entry.__room.instanceId === room.instanceId &&
                    entry.__room.homeId === room.homeId
                ) {
                    entry.send(json);
                } else {
                    console.log('User not in the current room');
                }
            });
        }
    } else if (json.type === "translate") {
        let msg = json.data.message.substring(3, json.data.message.length);
        translate(msg, {to: json.data.lang}).then(res => {
            //Translated text
            console.log(res.text);
            //Translated from
            console.log(res.from.language.iso);

            let translation = {
                type: 'translate.chat',
                message: 'Translation Response',
                data: {
                    from: res.from.language.iso,
                    to: json.data.lang,
                    translated: res.text,
                    original: msg,
                    corrected: res.from.text.value,
                    didYouMean: res.from.text.didYouMean,
                    autoCorrected: res.from.text.autoCorrected,
                    serverUserId: json.data.serverUserId,
                    chatType: json.data.serverUserId === 0 ? "C::" : "P::"
                }
            };
            connection.send(translation);
            Object.values(debugClients).forEach(function (entry) {
                if (json.type !== "ping") {
                    entry.send(translation);
                }
            });
        }).catch(err => {
            console.error(err);
        });
    } else if (json.type === "force_music") {
        Object.values(swfClients).forEach(function (entry) {
            entry.send(json)
        });
    } else if (json.type === "admin.alert.all") {
        if (swfClient.__user.level === 80085) {
            Object.values(swfClients).filter(function (entry) {
                return entry.__account.user_id !== swfClient.__account.user_id
            }).forEach(function (entry) {
                entry.send({
                    type: 'admin.alert',
                    message: 'Admin Alert from higher ups!',
                    help: 'Show the $data.envelope message first then $data.message',
                    envelope: {
                        title: 'Urgent!',
                        message: 'You received an important message from S1'
                    },
                    data: {
                        from: {
                            formal: 'The Higher Ups',
                            informal: swfClient.__account.user_name
                        },
                        title: 'S1 Msg',
                        message: json.data,
                        to: entry.__account.user_name,
                    }
                })
            });
        }
    } else if (json.type === "ping") {
        let pong = {type: 'pong', message: 'keep-alive'};
        connection.send(pong);
    } else if (json.type === "buddy.sync") {
        syncs[connection.__s1_token_acc] = json.data;
    } else if (json.type === "buddy.sync.add") {
        syncs[connection.__s1_token_acc].push(json.data);
    } else if (json.type === "buddy.sync.get") {
        let account_id = json.data;
        let clients = Object.values(swfClients).filter(function (client) {
            return client.__account.user_id == account_id;
        }).values();
        if (clients.length > 0) {
            let client = clients[0];
            let s1_token_account = client.__s1_token_acc;
            if (s1_token_account in syncs) {
                swfClient.send({
                    type: "buddy.sync.response",
                    message: "Getting buddies from " + client.__account.user_name,
                    data: syncs[s1_token_account]
                })
            } else {
                swfClient.send({type: "buddy.sync.response.error", message: "Users buddy list is empty", data: false})
            }
        }
    } else if (json.type === "buddy.sync.stop") {
        delete syncs[connection.__s1_token_acc]
    } else if (json.type === "buddy.sync.start") {
        syncs[connection.__s1_token_acc] = [];
    } else {
        /**
         * types: message, room, event, buddy_list_update, join_room
         */
        if (json.type === 'message' && connection.__type === 'swf') {
            var msg = json.data.message.substring(3, json.data.message.length);
            let del = msg.substring(0, 2);
            if (del === 'ﾳￅ') {
                msg = msg.substring(5, msg.length);
            }
            if (blockwords.indexOf(msg.toLocaleLowerCase()) > -1) {
                connection.send({
                    type: 'error',
                    message: 'Translation: This was a blockword'
                });
            } else {
                translate(msg, {to: 'en'}).then(res => {
                    if (res.from.language.iso !== 'en') {
                        //Translated text
                        console.log(res.text);
                        //Translated from
                        console.log(res.from.language.iso);


                        let translation = {
                            type: 'translation',
                            message: 'Translation Response',
                            data: {
                                fromLang: res.from.language.iso,
                                toLang: 'en',
                                translated: res.text,
                                original: msg,
                                corrected: res.from.text.value,
                                didYouMean: res.from.text.didYouMean,
                                autoCorrected: res.from.text.autoCorrected,
                                from: json.data.from,
                                to: json.data.to,
                                fromPlayerId: json.data.fromPlayerId,
                                fromPlayerName: json.data.fromPlayerName,
                                fromPlayerColor: json.data.fromPlayerColor,
                                chatType: json.data.chatType === 0 ? "C" : json.data.chatType === 1 ? "P" : "S"
                            }
                        };
                        connection.send(translation);
                        Object.values(debugClients).forEach(function (entry) {
                            if (json.type !== "ping") {
                                entry.send(translation);
                            }
                        });

                    } else {
                        connection.send({
                            type: 'error',
                            message: 'Language could bot be translated since the language is already in english'
                        });
                    }
                }).catch(err => {
                    console.error(err);
                });
            }
        }
        if (swfClient !== undefined && appClient !== undefined) {
            if (json.type === 'room') {
                swfClient.__room = json.data;
                appClient.__room = json.data;
            }
            if (connection.__type === 'swf') {
                appClient.send(json)
            } else if (connection.__type === 'app') {
                swfClient.send(json)
            }
        } else {

            if (connection.__type === 'swf') {
                connection.send({
                    type: 'error',
                    message: 'SupremeOne App isn\'t connected at the moment'
                });
            } else if (connection.__type === 'app') {
                connection.send({
                    type: 'error',
                    message: 'YoWorld isn\'t loaded at the moment'
                });
            }
        }
    }
    Object.values(debugClients).forEach(function (entry) {
        if (json.type !== "ping") {
            entry.send(json);
        }
    });
}

function handleDebug(connection, json) {
    let debugClient = debugClients[connection.__fd];
    let oauth = json.oauth;
    let s1_token_acc = oauth.s1_token_acc;
    let s1_token = oauth.s1_token;
    let swfClient = s1_token_acc in swfClients ? swfClients[s1_token_acc] : null;
    let appClient = s1_token in appClients ? appClients[s1_token] : null;
    switch (json.todo) {
        case "emit.admin.msg":
            Object.values(swfClients).forEach(function (entry) {
                entry.send({
                    type: 'admin.alert',
                    message: 'Admin Alert from higher ups!',
                    help: 'Show the $.envelope message first then $.data',
                    envelope: {
                        title: 'Urgent!',
                        message: 'You received an important message from S1'
                    },
                    data: {
                        from: {
                            formal: 'The Higher Ups',
                            informal: "The Higher Ups"
                        },
                        title: 'S1 Msg',
                        message: json.data,
                        to: entry.__account.user_name,
                    }
                })
            });
            break;
        case "emit.swf":
            if (swfClient !== null) {
                swfClient.send(json.data)
            }
            break;
        case "emit.app":
            if (appClient !== null) {
                appClient.send(json.data)
            }
            break;
    }
    Object.values(debugClients).forEach(function (entry) {
        entry.send({
            type: 'debug.emit',
            message: 'Packet Sent',
            data: json.data
        });
    });
}

function process(connection, json) {
    try {
        //console.log('DATA', socket.remoteAddress, ': ', data, typeof data, "===", typeof "exit");
        if (json.auth !== undefined) {
            if (handleAuth(connection, json)) {
                let appClient = appClients[connection.__s1_token];
                let swfClient = swfClients[connection.__s1_token_acc];
                handleConnection(connection, appClient, swfClient, json);
            }
        } else if (json.type === 'init' && json.message === 'debug') {
            connection.__type = json.message;
            connection.__fd = connection.socket.fd;
            debugClients[connection.__fd] = connection
        } else if (json.message === 'debug' && json.todo !== undefined) {
            handleDebug(connection, json)
        } else if (json.type === "exit") {
            console.log('exit message received !');
            connection.close()
        } else {
            console.log("FAILED")
        }
    } catch (e) {
        try {
            console.error("CONTENT WAS NOT JSON OR DATA WAS TOO LARGE. PIPING NOW");
            console.error(e);
            connection.send({type: 'urgent', message: 'DATA WAS TOO LARGE', data: e});
            if (connection.__type === 'swf') {
                let app = appClients[connection.__s1_token];
                if (app !== undefined)
                    app.send(json);
            } else if (connection.__type === 'app') {
                let swf = swfClients[connection.__s1_token_acc];
                if (swf !== undefined)
                    swf.send(json);
            }
        } catch (ex) {
            console.error("FAILED TO PIPE");
            console.error(ex);
        }
    }
}


function onConnectionClose(connection) {
    if (connection.__type === undefined) {
        console.log("Removed Unknown Socket");
        Object.values(debugClients).forEach(function (entry) {
            entry.send({
                type: 'debug.disconnected.unknown',
                message: 'Unknown',
                data: 'Unknown socket removed'
            });
        });
        return;
    }
    if (connection.__type === "swf") {
        Object.values(debugClients).forEach(function (entry) {
            entry.send({
                type: 'debug.disconnected',
                message: 'swf',
                data: connection.__account
            });
        });
        delete swfClients[connection.__s1_token_acc];
        delete syncs[connection.__s1_token_acc];
    } else if (connection.__type === "app") {
        Object.values(debugClients).forEach(function (entry) {
            entry.send({
                type: 'debug.disconnected',
                message: 'app',
                data: connection.__user
            });
        });
        delete appClients[connection.__s1_token];
    } else if (connection.__type === 'debug') {
        delete debugClients[connection.__fd];
    }
}

function buddySync() {
    let keys = Object.keys(syncs);
    if (keys.length > 0) {
        keys.forEach(function (s1_token_acc) {
            try {
                let client = s1_token_acc in swfClients ? swfClients[s1_token_acc] : null;
                if (client != null) {
                    let payload_request = {
                        type: 'buddy.sync.request',
                        'message': 'Can I haz buddies?',
                        data: 'send back a list of ur buddies to confirm thanx.'
                    };
                    client.send(payload_request);
                }
            } catch (e) {
                console.error(e);
            }
        });

        Object.values(debugClients).forEach(function (entry) {
            entry.send({
                type: 'debug.buddy-sync',
                message: 'Payload Request Sent',
                data: "Sent buddies requests to: " + keys.length.toString() + " clients."
            });
        });
    }
}

const server = net.createServer(function (socket) {
    try {
        const connection = new Connection(socket);
        socket.on('close', function () {
            onConnectionClose(connection)
        });
        connection.on('message', function (message) {
            process(connection, message)
        });
        socket.on('error', console.error)
    } catch (e) {
        console.error(e)
    }
});

const policy = net.createServer(function (socket) {
    socket.setEncoding("utf8");
    console.log('Policy Server received request from ', socket.remoteAddress, ' and responded with Socket Policy');
    socket.write(policyXml + '\u0000');
    socket.on('close', function () {
        console.log('Policy client closed: ', socket.remoteAddr);
    });
    socket.on('data', function (data) {
        console.log("POLICY RESPONSE: " + data);
    });
    socket.on('error', console.error)
});


policy.on('error', function (error) {
});
server.listen(2002, function () {
    console.log("SUPREME SERVER STARTED: ", (new Date()).toJSON().slice(0, 19).replace(/[-T]/g, ':'));
});
policy.listen(843, function () {
    console.log("POLICY SERVER STARTED: ", (new Date()).toJSON().slice(0, 19).replace(/[-T]/g, ':'));
});

function connectedClients() {
    let swf = Object.values(swfClients);
    let app = Object.values(appClients);
    let debug = Object.values(debugClients);
    if (swf.length > 0 || app.length > 0 || debug.length > 0) {
        let swfAccounts = swf.map(function (item) {
            return {name: item.__account.user_name, id: item.__account.user_id};
        });
        let appAccounts = app.map(function (item) {
            return {name: item.__user.name, id: item.__user.id};
        });

        let payload = {
            clients: {swf: swf.length, app: app.length, debug: debug.length},
            swf: swfAccounts,
            app: appAccounts
        };
        Object.values(debugClients).forEach(function (entry) {
            entry.send({
                type: 'debug.connect-clients',
                message: 'Connected Clients',
                data: payload
            });
        });
    }
}

setInterval(connectedClients, 1000 * 30);
setInterval(buddySync, 1000 * 30);

