#Supreme1 ServerSocket is built in Node.
###This project is a node socket server that the swf & the app connects to.
###The swf authenicate itself with my socket server by passing in tokens in which the socket server responds OK or disconnects the user.
###This nodejs server is primarily used for SWF <-> Node and SWF <-> Node <-> Android communication.

##Features
###Translate english to any language using google translate api. (This way we can translate conversations within the swf or talk to users in their language)
###Transmit conversation (Being able to send a message or recieve a message - this feature is used within the android app)
###Retrieve Character within the room (We use this feature to send ingame private messages based off their room character id)
###Theres other unimportant features implemented in the game.